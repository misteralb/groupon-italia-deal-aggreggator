<?php

// WP BOOTSTRAP
require '../../wp-blog-header.php';
// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
require '../../wp-admin/includes/image.php';

// global $current_user;
// get_currentuserinfo();
// print_r($current_user);

function create_affiliate_url($url) {
    $affiliate_url = 'http://clkuk.tradedoubler.com/click?p=264869&a=2354473&g=0&http://t.groupon.it/r?url=' . $url . '?utm_medium=afl&utm_source=TDB&utm_campaign=IT_DT_AFL_TDB_TDB_TIM_TTT_SR_LBP_CHA_YBR_pid*[td_affiliate_id]_uji*^^';
    return $affiliate_url;
}

// Attach Image on Deal
function add_deal_images($post_id, $deal_image) {
	// Download image during parsing
	$image = file_get_contents($deal_image);
	// Wordpress Upload Dirs
	$upload_dir = wp_upload_dir();
	$upload_path = $upload_dir['path'];
	$upload_url = $upload_dir['url'];
	// Image Filename
	$image_name = $post_id . '-' . basename($deal_image);
	// Download File from external server to our server
	$upload_file = $upload_path . "/" . $image_name;
	file_put_contents($upload_file, $image);
	$deal_image = $upload_url . "/" . $image_name;

	// $filename should be the path to a file in the upload directory.
	$filename = $upload_file;
	// The ID of the post this attachment is for.
	$parent_post_id = $post_id;
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype(basename($filename), null);

	// Prepare an array of post data for the attachment.
	$attachment = array(
	    'guid'           => $deal_image, 
	    'post_mime_type' => $filetype['type'],
	    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($filename)),
	    'post_content'   => '',
	    'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
	wp_update_attachment_metadata($attach_id, $attach_data);

	$dealImage = array($attach_id, $deal_image);
	return $dealImage;
}

if(isset($_GET['createdeal'])) {

	function dealsavetowp() {
		// CONSTANTS
		$author_id = "24";
		$deal_category_id = "12";
		$deal_gender = "Entrambi";
		$deal_age_groups = array('All Age Ranges');
		$seller_name = "Groupon";

		$deal_title = htmlentities($_GET['dealtitle']);
		$url = $_GET['url'];
		$url = create_affiliate_url($url);
		$dealdescription = htmlentities($_GET['dealdescription']);
		$currentdate = $_GET['currentdate'];
		$dealenddate = $_GET['dealenddate'];
		$originalprice = $_GET['originalprice'];
		$discountedprice = $_GET['discountedprice'];
		$dealcity = $_GET['dealcity'];
		$dealimage = $_GET['dealimage'];

		$deal_location = get_term_by('name', $dealcity, 'deals_city');
		$deal_location_id = (int) $deal_location->term_id;
		if (!$deal_location_id) $deal_location_id = '52';

		// Save Deal Data to DB
		$page_check = get_page_by_title($deal_title, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'publish', 'post_title' => $deal_title));

			add_post_meta($post_id, 'seller_name', $seller_name);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			add_post_meta($post_id, 'affiliate_link', $url);
			add_post_meta($post_id, 'deal_description', $dealdescription);
			add_post_meta($post_id, 'deal_start_date', $currentdate);
			add_post_meta($post_id, 'deal_end_date', $dealenddate);
			add_post_meta($post_id, 'original_price', $originalprice);
			add_post_meta($post_id, 'discounted_price', $discountedprice);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

			$deal_image_data = add_deal_images($post_id, $dealimage);
			add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

			echo "Deal <b>\"" . $deal_title . "\"</b> has been created successfully!<br/>";
		}else{
		    echo "Deal <b>\"" . $deal_title . "\"</b> already exists!<br/>";
		}

	}

	if(is_user_logged_in()) {
		global $current_user;
		get_currentuserinfo();
		if($current_user->roles['0'] == 'administrator') dealsavetowp();
	} else {
		echo "<b>You have no authorization to save data into WP!</b>";
	}

}