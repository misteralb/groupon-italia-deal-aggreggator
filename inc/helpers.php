<?php

function sanitize($input) {
	$input = htmlspecialchars($input);
	return $input;
}

function calculate_time($time) {
    if($time != 'Solo per poco tempo!' && $time != '') {
        $current_time = time();
        $deal_end_time = str_replace('giorni' , 'days', $time);
        $deal_end_date = strtotime($deal_end_time);
        $deal_end_date = gmdate("d/m/Y", $deal_end_date);
        return $deal_end_date;
    } else {
        return 'Non disponibile!';
    }
}

function get_current_date() {
    $current_date = time();
    $current_date = gmdate('d/m/Y', $current_date);
    return $current_date;
}