<?php

require 'helpers.php';
require 'wp.php';

if(isset($_GET['url'])) {
	$url = sanitize($_GET['url']);
	parsedealdata($url);
} else {
	echo "Invalid command!";
}

function parsedealdata($url) {
	$dom = new DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTMLFile($url);
    $DOMxpath = new DOMXPath($dom);

	$data = $DOMxpath->query("//h1[@class='deal-page-title']");
	$dealtitle = $data->item(0)->textContent;
    $dealtitle = str_replace('?', '', utf8_decode($dealtitle));

	$images = $dom->getElementsByTagName('img');
    foreach ($images as $image) {
        if($image->getAttribute('src') != ''  && basename($image->getAttribute('src')) == 'c700x420.jpg') {
            $dealimages[] = $image->getAttribute('src');
        }
    }
    $dealimage = $dealimages['0'];

    $data = $DOMxpath->query("//td[@class='discount-value']");
    $originalprice = $data->item(0)->textContent;
    $originalprice = preg_replace("/[^0-9\,]/", "", $originalprice);

	$data = $DOMxpath->query("//div[@id='deal-hero-price']");
	$discountedprice = $data->item(0)->textContent;
    $discountedprice = preg_replace("/[^0-9\,]/", "", $discountedprice);

    $data = $DOMxpath->query("//li[@class='countdown-timer']");
    $dealenddate = $data->item(0)->textContent;
    $dealenddate = calculate_time($dealenddate);

	$data = $DOMxpath->query("//div[@itemprop='description']");
	$dealdescription = $data->item(0)->textContent;
    $dealdescription = str_replace('?', '', utf8_decode($dealdescription));

    $data = $DOMxpath->query("//a[@data-bhw='GoToMerchantPlacePageFromLocationsList']");
    $dealcity = $data->item(0)->textContent;
    if(!$dealcity) $dealcity = "Italia";

    $dealcontent = array($dealtitle, $dealimage, $originalprice, $discountedprice, $dealenddate, $dealdescription, $dealcity);
    if($dealcontent) prepareform($dealcontent);

}

function prepareform($dealcontent) { ?>
    
    <div class="row control-group">
        <div class="form-group col-md-12">
            <input type="hidden" class="form-control" id="currentdate" name="currentdate" value="<?php echo get_current_date(); ?>">
        </div>
        <div class="form-group col-md-12">
            <label for="dealtitle">Deal Title</label>
            <input type="text" class="form-control" id="dealtitle" name="dealtitle" value="<?php echo trim($dealcontent['0']); ?>">
        </div>
        <div class="form-group col-md-8">
            <label for="dealimage">Deal Image</label>
            <input type="text" class="form-control" id="dealimage" name="dealimage" value="<?php echo $dealcontent['1']; ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="dealcity">Deal City</label>
            <input type="text" class="form-control" id="dealcity" name="dealcity" value="<?php echo trim($dealcontent['6']); ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="originalprice">Original Price</label>
            <input type="text" class="form-control" id="originalprice" name="originalprice" value="<?php echo trim($dealcontent['2']); ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="discountedprice">Discounted Price</label>
            <input type="text" class="form-control" id="discountedprice" name="discountedprice" value="<?php echo trim($dealcontent['3']); ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="dealenddate">Deal End Date</label>
            <input type="text" class="form-control" id="dealenddate" name="dealenddate" value="<?php echo trim($dealcontent['4']); ?>">
        </div>
    </div>
    <div class="row control-group">
        <div class="form-group col-md-12">
            <label for="dealdescription">Deal Description</label>
            <textarea class="form-control" id="dealdescription" name="dealdescription"><?php echo trim($dealcontent['5']); ?></textarea>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <button type="submit" id="createdeal" class="btn btn-success">Create Deal</button>
        </div>
    </div>

    <script type="text/javascript">
        $('#createdeal').click(function(){

            var url = $('#url').val();
            var currentdate = $('#currentdate').val();
            var dealtitle = $('#dealtitle').val();
            var dealimage = $('#dealimage').val();
            var originalprice = $('#originalprice').val();
            var discountedprice = $('#discountedprice').val();
            var dealcity = $('#dealcity').val();
            var dealenddate = $('#dealenddate').val();
            var dealdescription = $('#dealdescription').val();

            $.ajax({
                type: 'GET',
                url: 'inc/wp.php',
                data: {createdeal:true, url:url, currentdate:currentdate, dealtitle:dealtitle, dealimage:dealimage, originalprice:originalprice, discountedprice:discountedprice, dealcity:dealcity, dealenddate:dealenddate, dealdescription:dealdescription},
                success:function(result){
                    $('#wpresult').html(result);
                },
                error: function (textStatus, errorThrown) {
                    $('#wpresult').html(errorThrown);
                }
            });

        });
    </script>

    <?php
}