<!DOCTYPE html>
<html lang="it-IT">

<head>
    <meta charset="utf-8">
    <title>Groupon Deal Aggreagator</title>
    <meta name="viewport" content="width=device-width" />
    <!-- Included CSS Files -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    <style>
    	/*GENERAL*/
		.container { padding: 20px; }
		textarea { min-height: 200px; max-height: 200px; min-width: 100%; max-width: 100%; }
    </style>
</head>

<body>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="input-group input-group-lg">
                    <input type="text" id="url" class="form-control input-lg" placeholder="enter deal url ...">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-lg" id="go" type="submit">Get Deal Data</button>
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id="result"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id="wpresult"></div>
            </div>
        </div>

    </div>

    <!-- Included JS Files -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {

			$('#url').focus();
			
			// when user clicks go ...
			$('#go').click(function(){
				var url = $('#url').val();

				$.ajax({
					type: 'GET',
					url: 'inc/groupon.php',
					data: {url:url},
					success:function(result){
						$('#result').html(result);
					},
					error: function (textStatus, errorThrown) {
		                $('#result').html(errorThrown);
		            }
				});

			});

			// when user presses enter key on keyboard ...
			$('#url').keydown(function(event) {
				if (event.which == 13) {
		   			$('#go').trigger('click');
		  		}
			});
		 
		});
    </script>
</body>

</html>